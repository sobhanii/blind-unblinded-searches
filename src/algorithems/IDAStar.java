package algorithems;

import models.State;

import java.util.*;

public class IDAStar {

    public static void solve2(State start, int threshhold) {
        int count = 0;
        if (start.isFinal()) {
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        PriorityQueue<State> fringe = new PriorityQueue<>(new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                int f1 = o1.getfCost(), f2 = o2.getfCost();
                if (f1 > f2) return 1;
                if (f1 < f2) return -1;
                else return 0;
            }
        });
        start.setgCost(0);
        start.setH_cost(start.heuristic());
        fringe.add(start);
        int min = Integer.MAX_VALUE;
        while (!fringe.isEmpty()) {
            State temp = fringe.poll();
            if (visitedList.contains(temp)) {
                continue;
            }
            visitedList.add(temp);
            count++;
            if (temp.isFinal()) {
                temp.print();
                System.out.println("node count = " + count);
                return;
            } else if (temp.getfCost() <= threshhold) {
                ArrayList<State> children = temp.makeChild();
                for (State child : children) {
                    child.setgCost(temp.getgCost() + 1);
                    child.setH_cost(child.heuristic());
                }
                fringe.addAll(children);
            }
            else{
                if (temp.getfCost() < min){
                    min = temp.getfCost();
                }
            }
        }

        System.out.println("not found" + " threshold: " + threshhold);
        solve2(start,min);

    }

}
