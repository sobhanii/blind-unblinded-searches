package algorithems;

import models.State;

public class IterativeDeepeningAStar {

    public static void solve(State start){
        //heuristic for problem
        int heuristic = start.heuristic();
        while (true){
            System.out.println("threshhold: " + heuristic);
            Result result = search(start,0,heuristic,0);
            if (result.type == 1){
                return;
            }
            else if (result.type == 2){
                int min = (int)result.result1;
                if (min == Integer.MAX_VALUE){
                    System.out.println("not found");
                    return;
                }
            }
            heuristic = (int) result.result1;
        }
    }

    public static Result search(State state,int g,int threshhold,int sysout_level){
        Result result = null;
        int f = state.getH_cost() + g;
        if (f > threshhold){
            result = new Result();
            result.type = 2;
            result.result1 = new Integer(f);
            return result;
        }
        if (state.getH_cost() == 0){
            result = new Result();
            result.type = 1;
            return result;
        }
        int min = Integer.MIN_VALUE;
        for (State child:state.makeChild()) {
            Result result1 = search(child,g+child.getgCost(),threshhold,sysout_level+1);
            if (result1.type ==1){
                child.print();
                return result1;
            }
            else if (result1.type == 2){
                int newMin = (Integer) result1.result1;
                if (newMin < min){
                    min = newMin;
                }
            }
        }
        result = new Result();
        result.type = 2;
        result.result1 = new Integer(min);
        return result;
    }

     static class Result{
        public int type;
        public Object result1;
        public Object result2;
    }



}
