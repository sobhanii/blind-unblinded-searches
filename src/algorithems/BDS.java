package algorithems;

import models.Board;
import models.State;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class BDS {

    private static State finalState;

    private static void getDestination(State start) {
        BFS.solve(start);
        Board boardReverse = BFS.answer.getBoard();
        finalState = new State(boardReverse);
    }

    public static void solve(State start) {
        int count = 0;

        getDestination(start);
        if (start.isFinal()) {
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        Queue<State> fringe = new LinkedList<>();

        Set<State> visitedListR = new TreeSet<>();
        Queue<State> fringeR = new LinkedList<>();

        fringe.add(start);
        fringeR.add(finalState);

        int curDepth = 0;
        int curDepthR = 0;
        boolean found = false;

        while (!found) {
            while (!fringe.isEmpty()) {
                State temp = fringe.peek();
                if(temp.getDepth()>curDepth){
                    curDepth++;
                    break;
                }
                temp = fringe.poll();
                if (visitedList.contains(temp)) {
                    continue;
                }
                visitedList.add(temp);
                count++;
                fringe.addAll(temp.makeChild());
            }

            Object[] fringeArr =  fringe.toArray();
            Object[] fringeArrR = fringeR.toArray();
            for (Object node : fringeArr) {
                for (Object nodeR : fringeArrR) {
                    if (((State)node).getBoard().equals(((State)nodeR).getBoard())) {
                        found = true;
                        System.out.println("Ashke shadie shamo negah kon "+ curDepth + "  " + curDepthR);
                        return;
                    }
                }
            }

            while (!fringeR.isEmpty()){
                State tempR = fringeR.peek();
                if(tempR.getDepth()>curDepthR){
                    curDepthR++;
                    break;
                }
                tempR = fringeR.poll();
                if (visitedListR.contains(tempR)) {
                    continue;
                }
                visitedListR.add(tempR);
                count++;
                fringeR.addAll(tempR.makeChild());
                fringeR.addAll(tempR.makeChildReverse());
            }


            fringeArr =  fringe.toArray();
            fringeArrR = fringeR.toArray();
            for (Object node : fringeArr) {
                for (Object nodeR : fringeArrR) {
                    if (((State)node).getBoard().equals(((State)nodeR).getBoard())) {
                        found = true;
                        System.out.println("answer: "+ curDepth + "  " + curDepthR);
                        return;
                    }
                }
            }

        }
        if(!found) System.out.println("not found");
    }

}
