package algorithems;

import models.State;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class IDS {
    public static void solve(State start){
        int depth = 0;
        int limit = 0;
        Set<State> visitedList = new TreeSet<>();
        Queue<State> fringe = new LinkedList<>();
        fringe.add(start);
        while (!fringe.isEmpty()){
            State current = fringe.poll();
            depth = current.getDepth();
            if (depth<=limit){
                if (current.isFinal()){
                    current.print();
                    return;
                }
                if (visitedList.contains(current)){
                    continue;
                }
                else{
                    visitedList.add(current);
                    fringe.addAll(current.makeChild());
                }
                if (depth == limit){
                    limit++;
                }
            }
        }
    }
}
