package algorithems;

import models.State;

import java.util.*;

public class DFS {
    public static void solve(State start){
        int count = 0;
        if (start.isFinal()){
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        //using stack as LIFO Queue
        Stack<State> fringe = new Stack<>();
        fringe.add(start);

        while(!fringe.isEmpty()){
            State current = fringe.pop();
            if (visitedList.contains(current)){
                continue;
            }
            visitedList.add(current);
            if (current.isFinal()){
                current.print();
                return;
            }
            else{
                visitedList.add(current);
                count++;
                fringe.addAll(current.makeChild());
            }
        }
        System.out.println("Solution not found");
    }
}
