package algorithems;

import models.State;

import java.util.*;

public class AStar {


    public static void solve(State start){
        Set<State> visitedList = new TreeSet<>();
        PriorityQueue<State> fringe = new PriorityQueue<State>(1, new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                if (o1.getfCost() > o2.getfCost()){
                    return 1;
                }
                else if(o1.getfCost() < o2.getfCost()){
                    return -1;
                }
                else{
                    return 0;
                }
            }
        });
        start.setgCost(0);
        start.setH_cost(start.heuristic());
        fringe.add(start);
        boolean found = false;

        while (!fringe.isEmpty() && (!found)){
            // lowest f_cost
            State current = fringe.poll();
            if (visitedList.contains(current)) {
                continue;
            }
            visitedList.add(current);
            if (current.isFinal()){
                found = true;
                current.print();
                return;
            }
            for (State child:current.makeChild()) {
                child.setgCost(current.getgCost() + 1);
                child.setH_cost(child.heuristic());

                if (fringe.contains(child)){
                    fringe.remove(child);
                }
                fringe.add(child);
                }
            }
        }


    public static void solve2(State start) {
            int count = 0;
            if (start.isFinal()) {
                start.print();
                return;
            }

            Set<State> visitedList = new TreeSet<>();
            PriorityQueue<State> fringe = new PriorityQueue<>(new Comparator<State>() {
                @Override
                public int compare(State o1, State o2) {
                    int f1 = o1.getfCost(), f2 = o2.getfCost();
                    if (f1 > f2) return 1;
                    if (f1 < f2) return -1;
                    else return 0;
                }
            });
            start.setgCost(0);
            start.setH_cost(start.heuristic());
            fringe.add(start);
            while (!fringe.isEmpty()) {
                State temp = fringe.poll();
                if (visitedList.contains(temp)) {
                    continue;
                }
                visitedList.add(temp);
                count++;
                if (temp.isFinal()) {
                    temp.print();
                    System.out.println("node count = " + count);
                    return;
                }
                ArrayList<State> children = temp.makeChild();
                for (State child : children) {
                    child.setgCost(temp.getgCost() + 1);
                    child.setH_cost(child.heuristic());
                }
                fringe.addAll(children);
            }

            System.out.println("not found");

        }


    }
