package algorithems;

import models.State;

import java.util.*;

public class GBFS {
    public static void solve(State start) {
        Set<State> visitedList = new TreeSet<>();
        PriorityQueue<State> fringe = new PriorityQueue<>(1, new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                if (o1.getH_cost() > o2.getH_cost()) {
                    return 1;
                } else if (o1.getH_cost() < o2.getH_cost()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        fringe.add(start);
        while (!fringe.isEmpty()) {
            State current = fringe.remove();
            if (current.isFinal()) {
                current.print();
                return;
            }
            fringe.add(current);
            for (State child : current.makeChild()) {
                if (!visitedList.contains(child) && !fringe.contains(child)) {
                    fringe.add(child);
                } else {
                    fringe.add(child);
                }
            }
        }
    }

    public static void solve2(State start) {
        int count = 0;
        if (start.isFinal()) {
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        PriorityQueue<State> fringe = new PriorityQueue<>(new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                int f1 = o1.getH_cost(), f2 = o2.getH_cost();
                if (f1 > f2) return 1;
                if (f1 < f2) return -1;
                else return 0;
            }
        });
        start.setgCost(0);
        start.setH_cost(start.heuristic());
        fringe.add(start);
        while (!fringe.isEmpty()) {
            State temp = fringe.poll();
            if (visitedList.contains(temp)) {
                continue;
            }
            visitedList.add(temp);
            count++;
            if (temp.isFinal()) {
                temp.print();
                System.out.println("node count = " + count);
                return;
            }
            ArrayList<State> children = temp.makeChild();
            for (State child : children) {
                child.setgCost(temp.getgCost() + 1);
                child.setH_cost(child.heuristic());
            }
            fringe.addAll(children);
        }

        System.out.println("not found");

    }
}

