package algorithems;

import models.State;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;

public class BFS {
    public static State answer;
    public static Queue<State> superFringe;
    public static Set<State> superVisitedList;

    public static void solve(State start) {

        int count = 0;
        if (start.isFinal()) {
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        Queue<State> fringe = new LinkedList<>();
        fringe.add(start);

        while (!fringe.isEmpty()) {
            State temp = fringe.poll();
            if (visitedList.contains(temp)){
                continue;
            }
            visitedList.add(temp);
            count++;
            if (temp.isFinal()) {
                answer = temp;
                temp.print();
                System.out.println("node count = " + count);
                return;
            }

            fringe.addAll(temp.makeChild());


        }

        superFringe = fringe;
        superVisitedList = visitedList;
        System.out.println("not found");

    }

}
