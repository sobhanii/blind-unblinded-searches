package algorithems;

import models.State;

import java.util.ArrayList;
import java.util.Set;
import java.util.TreeSet;

public class RBFS {

    public static void solve(State start) {

        if (start.isFinal()) {
            start.print();
            return;
        }

        RBFS rbfs = new RBFS();
        Set<State> states = new TreeSet<>();
        states.add(start);
        rbfs.solve(states, start, start.getChildMinCost());

    }

    public int solve(Set<State> visited, State current, int limit) {

        if (current.isFinal()) {
            current.print();
            return -1;
        }

        int child_limit = Integer.MAX_VALUE;
        int child_limit_second = Integer.MAX_VALUE;
        ArrayList<State> children = current.makeChild();
        ArrayList<State> toRemove = new ArrayList<>();
        for (State state : children) {
            state.setgCost(current.getgCost() + 1);
            if (visited.contains(state)) {
                toRemove.add(state);
            }
            if(state.isFinal()){
                state.print();
                return -1;
            }
        }
        for (State state : toRemove) {
            children.remove(state);
        }
        State minChild = null;
        while (true) {
            child_limit = Integer.MAX_VALUE;
            child_limit_second = Integer.MAX_VALUE;
//            System.out.println("depth = " + current.getDepth());
            for (State state : children) {
//                System.out.println("value = " + state.getChildMinCost());
                if (state.getChildMinCost() < child_limit) {
                    if (child_limit < child_limit_second) {
                        child_limit_second = child_limit;
                    }
                    child_limit = state.getChildMinCost();
                } else if (state.getChildMinCost() < child_limit_second) {
                    child_limit_second = state.getChildMinCost();
                }
            }
            child_limit_second = Math.min(limit, child_limit_second);
            if (child_limit > limit) {
                return child_limit;
            }
            for (State state : children) {
                if (child_limit == state.getChildMinCost()) {
                    minChild = state;
                    break;
                }
            }
            if (minChild == null) {
                return Integer.MAX_VALUE;
            }
            visited.add(current);
            int ans = solve(visited, minChild, child_limit_second);
            visited.remove(current);
            if (ans == -1) {
                return -1;
            }
            minChild.setChildMinCost(ans);
        }

    }

}

