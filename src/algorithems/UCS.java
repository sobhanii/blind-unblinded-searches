package algorithems;

import models.State;

import java.util.*;

public class UCS {
    public static void solve(State start){
        int pathCost = 0;
        boolean found = false;
        Queue<State> fringe = new LinkedList<>();
        Set<State> visitedList = new TreeSet<>();

        fringe.add(start);
        while (!fringe.isEmpty() && !found){
            State current = fringe.poll();
            visitedList.add(current);
            if (current.isFinal()){
                current.print();
                found = true;
            }
            else{
                ArrayList<State> children = current.makeChild();
                for (int i=0; i<children.size(); i++) {
                    State child = children.get(i);
                    if (child.getAction()==2 | child.getAction()==3){
                        child.setCost(current.getCost() + 3);
                    }
                    else{
                        child.setCost(current.getCost() + 1);
                    }
                    if (!visitedList.contains(child) && !fringe.contains(child)){
                        child.setParent(current);
                        fringe.add(child);
                    }
                    else if (fringe.contains(child) && child.getCost()>current.getCost()){
                        child.setParent(current);
                        fringe.remove(child);
                        fringe.add(child);
                    }
                }
            }
        }
    }

    public static void solve2(State start){
        int count = 0;
        if (start.isFinal()) {
            start.print();
            return;
        }

        Set<State> visitedList = new TreeSet<>();
        PriorityQueue<State> fringe = new PriorityQueue<>(new Comparator<State>() {
            @Override
            public int compare(State o1, State o2) {
                int f1 = o1.getgCost(), f2 = o2.getgCost();
                if (f1 > f2) return 1;
                if (f1 < f2) return -1;
                else return 0;
            }
        });
        start.setgCost(0);
        fringe.add(start);

        while (!fringe.isEmpty()) {
            State temp = fringe.poll();
            if (visitedList.contains(temp)) {
                continue;
            }
            visitedList.add(temp);
            count++;
            if (temp.isFinal()) {
                temp.print();
                System.out.println("node count = " + count);
                System.out.println("cost : " +temp.getgCost());
                return;
            }
            ArrayList<State> successor_children = temp.makeChild();
            for (State child : successor_children) {
                if(child.getBoard().getCube().isStand())
                    child.setgCost(temp.getgCost()+3);
                else child.setgCost(temp.getgCost()+1);
                fringe.add(child);
            }

        }

        System.out.println("not found");
    }
}
