package models;

import java.util.ArrayList;
import java.util.Stack;

public class State implements Comparable<State> {

    public   int heuristic(){
        return  this.getBoard().getEmptySpace() /2;
    }

    private int cost;
    private int gCost = 0;
    private int fCost = 0;
    private int hCost= 0;
    private int childMinCost = -1;

    private int depth;
    private Board board;
    private State parent;
    private int action;
    //0 -> up
    //1 -> down
    //2 -> left
    //3 -> right

    public State(Board board) {
        this.board = board;
        this.depth = 0;
        this.parent = null;
    }

    private State(int depth, Board board, State parent, int action) {
        this.depth = depth + 1;
        this.board = board;
        this.parent = parent;
        this.action = action;
    }

    public ArrayList<State> makeChild() {
        ArrayList<State> states = new ArrayList<>();
        if (board.getCube().isStand()) {
            if (board.getCube().getX() >= 2) {
                Board tempBoard = board.moveUp();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 0));
            }
            if (board.getCube().getX() <= board.getHeight() - 3) {
                Board tempBoard = board.moveDown();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 2) {
                Board tempBoard = board.moveLeft();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 2));
            }
            if (board.getCube().getY() <= board.getLength() - 3) {
                Board tempBoard = board.moveRight();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 3));
            }
        } else {
            if (board.getCube().isHorizontal()) {
                if (board.getCube().getX() >= 1) {
                    Board tempBoard = board.moveUp();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 3) {
                    Board tempBoard = board.moveRight();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 3));
                }
            } else {
                if (board.getCube().getX() >= 2) {
                    Board tempBoard = board.moveUp();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 2) {
                    Board tempBoard = board.moveRight();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 3));
                }
            }
            if (board.getCube().getX() <= board.getHeight() - 2) {
                Board tempBoard = board.moveDown();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 1) {
                Board tempBoard = board.moveLeft();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 2));
            }

        }


        return states;
    }

    public boolean isFinal() {
        return board.isFinal();
    }

    public void print() {
        Stack<Integer> actions = new Stack<>();
        State temp = this;
        while (temp.parent != null) {
            actions.push(temp.action);
            temp = temp.parent;
        }
        System.out.println(actions.size());
        while (!actions.isEmpty()) {
            switch (actions.pop()) {
                case 0:
                    System.out.println("up");
                    break;
                case 1:
                    System.out.println("down");
                    break;
                case 3:
                    System.out.println("right");
                    break;
                case 2:
                    System.out.println("left");
                    break;
            }
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        State state = (State) o;
        return board.equals(state.board);
    }

    @Override
    public int compareTo(State state) {
        if (this.equals(state))
            return 0;
        return 1;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public State getParent() {
        return parent;
    }

    public void setParent(State parent) {
        this.parent = parent;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getgCost() {
        return gCost;
    }

    public void setgCost(int gCost) {
        this.gCost = gCost;
    }

    public int getfCost() {
        this.hCost = heuristic();
        return  + gCost;
    }

    public void setfCost(int fCost) {
        this.fCost = fCost;
    }

    public int getH_cost() {
        return hCost;
    }

    public void setH_cost(int hCost) {
        this.hCost = hCost;
    }

    public void setChildMinCost(int number){
        childMinCost = number;
    }

    public int getChildMinCost(){
        if (childMinCost == -1){
            childMinCost =  getfCost();
        }
        return childMinCost;
    }
    public ArrayList<State> makeChildReverse() {
        ArrayList<State> states = new ArrayList<>();
        if (board.getCube().isStand()) {
            if (board.getCube().getX() >= 2) {
                Board tempBoard = board.moveUpReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 0));
            }
            if (board.getCube().getX() <= board.getHeight() - 3) {
                Board tempBoard = board.moveDownReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 2) {
                Board tempBoard = board.moveLeftReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 2));
            }
            if (board.getCube().getY() <= board.getLength() - 3) {
                Board tempBoard = board.moveRightReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 3));
            }
        } else {
            if (board.getCube().isHorizontal()) {
                if (board.getCube().getX() >= 1) {
                    Board tempBoard = board.moveUpReverse();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 3) {
                    Board tempBoard = board.moveRightReverse();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 3));
                }
            } else {
                if (board.getCube().getX() >= 2) {
                    Board tempBoard = board.moveUpReverse();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 2) {
                    Board tempBoard = board.moveRightReverse();
                    if (tempBoard != null)
                        states.add(new State(depth, tempBoard, this, 3));
                }
            }
            if (board.getCube().getX() <= board.getHeight() - 2) {
                Board tempBoard = board.moveDownReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 1) {
                Board tempBoard = board.moveLeftReverse();
                if (tempBoard != null)
                    states.add(new State(depth, tempBoard, this, 2));
            }

        }


        return states;
    }


}
