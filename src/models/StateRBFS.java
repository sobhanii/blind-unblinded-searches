package models;

import java.util.ArrayList;
import java.util.Stack;

public class StateRBFS implements Comparable<StateRBFS> {

    public   int heuristic(){
        return  this.getBoard().getEmptySpace() /2;
    }

    private int cost;
    private int g_cost = 0;
    private int f_cost = 0;
    private int h_cost = 0;

    private int depth;
    private Board board;
    private StateRBFS parent;
    private int action;
    //0 -> up
    //1 -> down
    //2 -> left
    //3 -> right

    public StateRBFS(Board board) {
        this.board = board;
        this.depth = 0;
        this.parent = null;
    }

    private StateRBFS(int depth, Board board, StateRBFS parent, int action) {
        this.depth = depth + 1;
        this.board = board;
        this.parent = parent;
        this.action = action;
    }

    public ArrayList<StateRBFS> makeChild() {
        ArrayList<StateRBFS> states = new ArrayList<>();
        if (board.getCube().isStand()) {
            if (board.getCube().getX() >= 2) {
                Board tempBoard = board.moveUp();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 0));
            }
            if (board.getCube().getX() <= board.getHeight() - 3) {
                Board tempBoard = board.moveDown();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 2) {
                Board tempBoard = board.moveLeft();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 2));
            }
            if (board.getCube().getY() <= board.getLength() - 3) {
                Board tempBoard = board.moveRight();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 3));
            }
        } else {
            if (board.getCube().isHorizontal()) {
                if (board.getCube().getX() >= 1) {
                    Board tempBoard = board.moveUp();
                    if (tempBoard != null)
                        states.add(new StateRBFS(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 3) {
                    Board tempBoard = board.moveRight();
                    if (tempBoard != null)
                        states.add(new StateRBFS(depth, tempBoard, this, 3));
                }
            } else {
                if (board.getCube().getX() >= 2) {
                    Board tempBoard = board.moveUp();
                    if (tempBoard != null)
                        states.add(new StateRBFS(depth, tempBoard, this, 0));
                }
                if (board.getCube().getY() <= board.getLength() - 2) {
                    Board tempBoard = board.moveRight();
                    if (tempBoard != null)
                        states.add(new StateRBFS(depth, tempBoard, this, 3));
                }
            }
            if (board.getCube().getX() <= board.getHeight() - 2) {
                Board tempBoard = board.moveDown();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 1));
            }
            if (board.getCube().getY() >= 1) {
                Board tempBoard = board.moveLeft();
                if (tempBoard != null)
                    states.add(new StateRBFS(depth, tempBoard, this, 2));
            }

        }


        return states;
    }

    public boolean isFinal() {
        return board.isFinal();
    }

    public void print() {
        Stack<Integer> actions = new Stack<>();
        StateRBFS temp = this;
        while (temp.parent != null) {
            actions.push(temp.action);
            temp = temp.parent;
        }
        System.out.println(actions.size());
        while (!actions.isEmpty()) {
            switch (actions.pop()) {
                case 0:
                    System.out.println("up");
                    break;
                case 1:
                    System.out.println("down");
                    break;
                case 3:
                    System.out.println("right");
                    break;
                case 2:
                    System.out.println("left");
                    break;
            }
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StateRBFS state = (StateRBFS) o;
        return board.equals(state.board);
    }

    @Override
    public int compareTo(StateRBFS state) {
        if (this.equals(state))
            return 0;
        return 1;
    }

    public int getDepth() {
        return depth;
    }

    public void setDepth(int depth) {
        this.depth = depth;
    }

    public Board getBoard() {
        return board;
    }

    public void setBoard(Board board) {
        this.board = board;
    }

    public StateRBFS getParent() {
        return parent;
    }

    public void setParent(StateRBFS parent) {
        this.parent = parent;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public int getG_cost() {
        return g_cost;
    }

    public void setG_cost(int g_cost) {
        this.g_cost = g_cost;
    }

    public int getF_cost() {
        this.h_cost = heuristic();
        return h_cost + g_cost;
    }

    public void setF_cost(int f_cost) {
        this.f_cost = f_cost;
    }

    public int getH_cost() {
        return h_cost;
    }

    public void setH_cost(int h_cost) {
        this.h_cost = h_cost;
    }


}
